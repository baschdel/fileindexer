
#this script generates file information about a given file to store it in a file database
#NOTE:Look for the file in your database before adding it! it may already be there

#depends on
## uuidgen
## basename
## echo
## php
## md5sum
## sha3sum
## awk

urlencode() {
  php -r "echo urlencode(\"$1\");"
}

#arguments
file=$1  #the path of the file to index
place=$(urlencode "$2") #the virtual directory the file is in

#uuid=$(uuidgen)

filename=$(urlencode "$(basename "$file")")

#generate hashes
md5sum=$(md5sum "$file" | awk '{print $1}')   #beacause it is fast
sha384sum=$(sha384sum "$file" | awk '{print $1}') #beacause it is save

echo 'name:'$filename 'place:'$place 'hash:md5='$md5sum 'hash:sha384='$sha384sum


