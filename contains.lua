local s,file = ...

if file then
 io.input(file)
end


--exits with code 0 if line s exists in file
--exits with code 1 otherwise

while true do
 i = io.read()
 if not i then break end
 if i == s then os.exit() end
end
os.exit(1)
