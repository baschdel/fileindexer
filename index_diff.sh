newindex=$1
oldindex=$2

preprocess(){
	cat $1 | while read line; do echo $line | sed 's/ /\n/g' | sort | gawk '{line=line " " $0} END {print line}' ; done | sed 's/ *$//' | sed 's/^ //'  | uniq -u
}

#find the lines that are updated
{ echo "$(preprocess $newindex)" ; echo "$(preprocess $oldindex)" ; } | #make all lines that have the same content look the same by sorting them and making them uinique inide a file so that the duplicaes after that are in both files
lua unique.lua | #throw out the lines that are in both files (they haven't changed)
while read line ;
do
 if echo "$(preprocess $newindex)" | lua contains.lua "$line" ;  #if the line is only in the newindex file it has been added
 then
  echo + $line
 else
  echo - $line #else it ha ben removed
 fi
done

